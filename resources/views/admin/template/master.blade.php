<!DOCTYPE html>
<html lang="tr">
<head>

    <title>{{ config('app.name') }}@yield('title', '') </title>
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    @include('admin.template.modules.head.metadata')
    @include('admin.template.modules.head.style')
    @yield('head')

</head>

<body>

<!-- Top Bar Start -->
<div class="topbar">

    @include('admin.template.modules.body.logo')
    @include('admin.template.modules.body.navbar')

</div>
<!-- Top Bar End -->

<div class="page-wrapper">

@include('admin.template.modules.body.left-sidenav')

<!-- Page Content-->
    <div class="page-content">

        <div class="container-fluid">

            @include('admin.template.modules.body.breadcrumb')

            @yield('content')

        </div><!-- container -->

        @include('admin.template.modules.body.footer')

    </div>
    <!-- end page content -->
</div>
<!-- end page-wrapper -->

@include('admin.template.modules.body.script')
@yield('body')

</body>
</html>
