<!-- Left Sidenav -->
<div class="left-sidenav">
    <ul class="metismenu left-sidenav-menu">

        <li>
            <a href="javascript: void(0);"><i class="ti-crown"></i><span>Ürün Yönetimi</span><span class="menu-arrow"><i
                        class="mdi mdi-chevron-right"></i></span></a>
            <ul class="nav-second-level" aria-expanded="false">
                <li>
                    <a href="javascript: void(0);"><i class="ti-control-record"></i>Kategoriler<span
                            class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="#">Tüm Kategoriler</a></li>
                        <li><a href="#">Kategori Ekle</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="ti-control-record"></i>Ürünler <span
                            class="menu-arrow left-has-menu"><i class="mdi mdi-chevron-right"></i></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="#">Tüm Ürünler</a></li>
                        <li><a href="#">Ürün Ekle</a></li>
                        <li><a href="#">Ürün Bul</a></li>
                    </ul>
                </li>

            </ul>
        </li>

        <li>
            <a href="javascript: void(0);"><i class="ti-bar-chart"></i><span>Analytics</span><span
                    class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
            <ul class="nav-second-level" aria-expanded="false">
                <li class="nav-item"><a class="nav-link" href="#"><i
                            class="ti-control-record"></i>Dashboard</a></li>
                <li class="nav-item"><a class="nav-link" href="#"><i
                            class="ti-control-record"></i>Customers</a></li>
                <li class="nav-item"><a class="nav-link" href="#"><i
                            class="ti-control-record"></i>Reports</a></li>
            </ul>
        </li>


        <li>
            <a href="../analytics/analytics-index-dark-sidenav.html"><i
                    class="ti-palette"></i><span>Dark Sidenav</span></a>
        </li>
    </ul>
</div>
<!-- end left-sidenav-->
