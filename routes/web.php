<?php


Route::get('/', function () {
    return view('admin.template.master');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/kategoriler', 'CategoryController@index')->name('admin.kategori.index');

});
